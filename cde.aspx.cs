﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.Services;
using System.Web.Script.Services;

public partial class cde : System.Web.UI.Page
{
    int index = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, count(*) as SHGs, e.cdoname, e.cdmname from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on cast(s.cdeid as varchar(50)) = cast(e.cdeid as varchar(50)) group by e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, e.cdoname, e.cdmname", gvsprovider);
        }
    }
    public void cdoBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].cdo";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlcdo.DataSource = ds;
                ddlcdo.DataTextField = "cdoname";
                ddlcdo.DataValueField = "cdoid";
                ddlcdo.DataBind();
                ddlcdo.Items.Insert(0, new ListItem("Choose", "0"));
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void cdmBind()
    {
        try
        {
            string qur = "select * from [vikasardo].[vikasa].cdm";
            if (Alib.idHasRows(qur))
            {
                SqlConnection con = new SqlConnection(Alib.conStr);
                con.Open();
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlcdm.DataSource = ds;
                ddlcdm.DataTextField = "cdmname";
                ddlcdm.DataValueField = "cdmid";
                ddlcdm.DataBind();
                ddlcdm.Items.Insert(0, new ListItem("Choose", "0"));
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, count(*) as SHGs, e.cdoname, e.cdmname from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on cast(s.cdeid as varchar(50)) = cast(e.cdeid as varchar(50)) group by e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, e.cdoname, e.cdmname", gvsprovider);
    }
    protected void Button2_ServerClick(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        clean();
        btnsubmit.Text = "Submit";
        cdoBind();
        cdmBind();
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string rad = "";
            string fname = "";
            if (rad1.Checked != true)
            {
                if (rad2.Checked != true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Select Any Type Permanent/ Freelancer',text: '',timer: 2000,showConfirmButton: false})", true);
                    return;
                }
            }
            if (txtname.Text.Trim().Equals(""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Name',text: '',timer: 2000,showConfirmButton: false})", true);
                return;
            }
            if (txtcontact.Text.Trim().Equals(""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Contact Number',text: '',timer: 2000,showConfirmButton: false})", true);
                return;
            }
            if (txtemail.Text.Trim().Equals(""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter Email Address',text: '',timer: 2000,showConfirmButton: false})", true);
                return;
            }
            if (ddlcdo.SelectedItem.Text.Equals("Choose"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Choose CDO',text: '',timer: 2000,showConfirmButton: false})", true);
                return;
            }
            if (ddlcdm.SelectedItem.Text.Equals("Choose"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Choose CDM',text: '',timer: 2000,showConfirmButton: false})", true);
                return;
            }
            if (fpphoto.HasFile)
            {
                fname = fpphoto.FileName;
                fpphoto.SaveAs(Server.MapPath("~/images/") + fname);
            }
            if (rad1.Checked == true)
            {
                rad = rad1.Text.ToString();
            }
            else
            {
                rad = rad1.Text.ToString();
            }

            if (btnsubmit.Text == "Submit")
            {
                if (Alib.idHasRows("SELECT cdephone FROM [vikasardo].[vikasa].[cde] WHERE cdephone='" + txtcontact.Text + "'"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'CDE Exist With  '+'" + txtcontact.Text + "',text: '',timer: 3000,showConfirmButton: false})", true);
                    return;
                }
                else
                {
                    Alib.idInsertInto("[vikasardo].[vikasa].[cde]", "employment", rad.ToString(), "cdename", txtname.Text, "cdephone", txtcontact.Text, "cdemail", txtemail.Text, "cdoid", ddlcdo.SelectedValue, "cdmid", ddlcdm.SelectedValue, "cdephoto", fname, "cdoname", ddlcdo.SelectedItem.Text, "cdmname", ddlcdm.SelectedItem.Text);
                }
            }
            else
            {
                if (!fpphoto.HasFile)
                {
                    Alib.idExecute("Update [vikasardo].[vikasa].[cde] set employment= '" + rad.ToString() + "', cdename= '" + txtname.Text + "', cdephone= '" + txtcontact.Text + "', cdemail= '" + txtemail.Text + "', cdoid= '" + ddlcdo.SelectedValue + "', cdmid= '" + ddlcdm.SelectedValue + "', cdoname= '" + ddlcdo.SelectedItem.Text + "', cdmname= '" + ddlcdm.SelectedItem.Text + "' where cdeid='" + lblid.Text + "'");
                }
                else
                {
                    Alib.idExecute("Update [vikasardo].[vikasa].[cde] set employment= '" + rad.ToString() + "', cdename= '" + txtname.Text + "', cdephone= '" + txtcontact.Text + "', cdemail= '" + txtemail.Text + "', cdoid= '" + ddlcdo.SelectedValue + "', cdmid= '" + ddlcdm.SelectedValue + "', cdephoto= '" + fname + "', cdoname= '" + ddlcdo.SelectedItem.Text + "', cdmname= '" + ddlcdm.SelectedItem.Text + "' where cdeid='" + lblid.Text + "'");
                }
            }
        }
        catch (Exception ex)
        {
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Successfully Updated',text: '',timer: 2000,showConfirmButton: false})", true);
        Glib.LoadRequest("Select e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, count(*) as SHGs, e.cdoname, e.cdmname from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on cast(s.cdeid as varchar(50)) = cast(e.cdeid as varchar(50)) group by e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, e.cdoname, e.cdmname", gvsprovider);
        clean();
        return;
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
        clean();
    }
    public void clean()
    {
        btnsubmit.Text = "Submit";
        txtname.Text = txtemail.Text = txtcontact.Text = "";
        rad1.Checked = false;
        rad2.Checked = false;
        cdoBind();
        cdmBind();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select shgid as ID, name as Name, type as Type, village as Village, taluka as Taluka, region as Region, district as District,savingAccount as Saving_Acc,meetingDate as Meeting_Date,totalMember as Members,meetingPlace as Meeting_Place,meetingTime as Meeting_Time,formationDate as Formation_Date,savingAccOpenDate as Acc_Open_Date from [vikasardo].[vikasa].shg where cdeid='" + index + "'", GridView1);
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Del"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            Alib.idExecute("delete from [vikasardo].[vikasa].cde where cdeid=" + id);
            Glib.LoadRequest("Select e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, count(*) as SHGs, e.cdoname, e.cdmname from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on cast(s.cdeid as varchar(50)) = cast(e.cdeid as varchar(50)) group by e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, e.cdoname, e.cdmname", gvsprovider);
        }
        if (e.CommandName.Equals("edi"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            lblid.Text = id;
            MultiView1.ActiveViewIndex = 0;
            btnsubmit.Text = "Update";
            string radiobuttonText = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            if (radiobuttonText.ToString() == "Contract")
            {
                rad1.Checked = true;
            }
            else
            {
                rad2.Checked = true;
            }
            txtname.Text = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
            txtcontact.Text = HttpUtility.HtmlDecode(rows.Cells[5].Text).ToString();
            txtemail.Text = HttpUtility.HtmlDecode(rows.Cells[6].Text).ToString();
            cdoBind();
            cdmBind();
            ddlcdo.SelectedValue = HttpUtility.HtmlDecode(rows.Cells[7].Text).ToString();
            ddlcdm.SelectedValue = HttpUtility.HtmlDecode(rows.Cells[8].Text).ToString();
        }
        if (e.CommandName.Equals("view"))
        {
            index = Convert.ToInt32(e.CommandArgument);
            string qur = "Select shgid as ID, name as Name, type as Type, village as Village, taluka as Taluka, region as Region, district as District,savingAccount as Saving_Acc,meetingDate as Meeting_Date,totalMember as Members,meetingPlace as Meeting_Place,meetingTime as Meeting_Time,formationDate as Formation_Date,savingAccOpenDate as Acc_Open_Date from [vikasardo].[vikasa].shg where cdeid='" + index + "'";
            try
            {
                GridView1.DataSource = Alib.getData(qur);
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
            }
        }
    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            return;
        }
        else
        {
            Glib.LoadRequest("Select e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, count(*) as SHGs, e.cdoname, e.cdmname from [vikasardo].[vikasa].[cde] e inner join [vikasardo].[vikasa].[shg] s on cast(s.cdeid as varchar(50)) = cast(e.cdeid as varchar(50)) where e.cdename like '" + txtsrch.Text + "' or e.cdephone like '" + txtsrch.Text + "' group by e.cdeid, e.employment, e.cdename, e.cdephone, e.cdemail, e.cdoid, e.cdmid, e.cdephoto, e.cdoname, e.cdmname", gvsprovider);

        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select top 10 cdename as tmp from [vikasardo].[vikasa].[cde] where cdename like '" + pre + "%' union Select top 10 cdephone as tmp from [vikasardo].[vikasa].cde where cdephone like '" + pre + "%'";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
}