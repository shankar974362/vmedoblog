﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cde.aspx.cs" Inherits="cde" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <script src="assets/js/jquery.min.js"></script>
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

    <link href="assets/js/jquery.timepicker.css" rel="stylesheet" />
    <script src="assets/js/jquery.timepicker.js"></script>
    <script src="assets/js/jquery.timepicker.min.js"></script>
    <style type="text/css">
        .img-zoom {
            width: 30px;
            -webkit-transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            -ms-transition: all .2s ease-in-out;
        }

        .transition {
            -webkit-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
        }
    </style>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#bfs5').addClass('active');
            $('#id3').addClass('active');
            $('.img-zoom').hover(function () {
                $(this).addClass('transition');
            }, function () {
                $(this).removeClass('transition');
            });
            $('.fancybox').fancybox();
            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    overlay: {
                        speedOut: 0
                    }
                }
            });

            // Disable opening and closing animations, change title type
            $(".fancybox-effects-b").fancybox({
                openEffect: 'none',
                closeEffect: 'none',

                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });

            // Set custom style, close if clicked, change title type and overlay color
            $(".fancybox-effects-c").fancybox({
                wrapCSS: 'fancybox-custom',
                closeClick: true,
                openEffect: 'none',
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay: {
                        css: {
                            'background': 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });
        });
    </script>

    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }
    </style>
     <script>
         $(function () {
             $('#<%=txtsrch.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "cde.aspx/GetInvName",
                        data: "{ 'pre':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (d) {
                            var data = JSON.parse(d.d);
                            console.log(data);
                            response($.map(data, function (item) {
                                return { value: item.tmp }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">CDE'S</h4>
        </div>
    </div>
    <!--/ .page title -->

    <div class="row">
        <div class="col-sm-12 ">
            <p class="text-muted page-title-alt">
                <button type="button" id="Button2" style="float: right;" runat="server" class="btn btn-default waves-effect waves-light" onserverclick="Button2_ServerClick">
                    <span class="btn-label"><i class="fa fa-plus"></i>
                    </span>Create New</button>
            </p>
        </div>
         <div class="form-inline">
            <asp:TextBox ID="txtsrch" runat="server" class="form-control" placeholder="Enter name / Phone" Width="25%"></asp:TextBox>
            <asp:Button ID="btnsrch" runat="server" CssClass="form-control" Text="Search" Width="10%" OnClick="btnsrch_Click" />
        </div>
    </div>
    <br />
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="row">
                <div class="col-lg-6 card-box">
                    <div class="form-group">
                        <asp:RadioButton ID="rad1" runat="server" Text="Contract" GroupName="a" />
                        <asp:RadioButton ID="rad2" runat="server" Text="Freelancer" GroupName="a" Style="padding-left: 15px;" />
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Contact Number</label>
                        <asp:TextBox ID="txtcontact" runat="server" class="form-control"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" SetFocusOnError="True"
                            ControlToValidate="txtcontact" ErrorMessage="Must be Valid 10 digit" ForeColor="Red"
                            ValidationExpression="^[7-9][0-9]{9}$" Font-Size="Small"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-group">
                        <label>Email ID</label>
                        <asp:TextBox ID="txtemail" runat="server" class="form-control"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                            runat="server" ErrorMessage="Please Enter Valid Email ID"
                            ValidationGroup="vgSubmit" ControlToValidate="txtemail"
                            CssClass="requiredFieldValidateStyle"
                            ForeColor="Red"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="col-lg-6 card-box">
                    <div class="form-group">
                        <label>CDO</label>
                        <asp:DropDownList ID="ddlcdo" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>CDM</label>
                        <asp:DropDownList ID="ddlcdm" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>Photo</label>
                        <asp:FileUpload ID="fpphoto" runat="server"></asp:FileUpload>
                    </div>
                    <br />
                    <br />
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-lg btn-default btn-custom" Text="Submit" OnClick="btnsubmit_Click" />
                        <asp:Button ID="btncancel" runat="server" CssClass="btn btn-lg btn-danger btn-custom" Text="Cancel" OnClick="btncancel_Click" />
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </asp:View>
    </asp:MultiView>

    <div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" EmptyDataText="No Data Available" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped" OnPageIndexChanging="gvsprovider_PageIndexChanging"
                OnRowCommand="gvsprovider_RowCommand" AllowPaging="true" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:ButtonField CommandName="edi" ControlStyle-CssClass="btn btn-default btn-custom" ButtonType="Button"
                        Text="Edit" HeaderText="Edit"></asp:ButtonField>
                    <asp:ButtonField CommandName="Del" ControlStyle-CssClass="btn btn-danger btn-custom" ButtonType="Button"
                        Text="Delete" HeaderText="Delete"></asp:ButtonField>
                    <asp:BoundField DataField="cdeid" HeaderText="Identity"></asp:BoundField>
                    <asp:BoundField DataField="employment" HeaderText="Type"></asp:BoundField>
                    <asp:BoundField DataField="cdename" HeaderText="Name"></asp:BoundField>
                    <asp:BoundField DataField="cdephone" HeaderText="Contact"></asp:BoundField>
                    <asp:BoundField DataField="cdemail" HeaderText="Email"></asp:BoundField>
                    <asp:BoundField DataField="cdoid" HeaderText="CDO"></asp:BoundField>
                    <asp:BoundField DataField="cdmid" HeaderText="CDM"></asp:BoundField>
                    <asp:TemplateField HeaderText="Photo">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" data-fancybox-group="gallery" class="fancybox"
                                NavigateUrl='<%# Eval("cdephoto","images/{0}") %>'>
                                <asp:Image ID="picture" CssClass="img-zoom" runat="server" Width="50px" Height="50px" ImageUrl='<%# Eval("cdephoto","images/{0}") %>' />
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--<asp:BoundField DataField="cdephoto" HeaderText="Photo"></asp:BoundField>--%>
                    <asp:TemplateField HeaderText="SHGs &#8794;">
                        <ItemTemplate>
                            <asp:Button ID="Button1" runat="server" CommandName="view" Text='<%#Eval("SHGs")%>' DataField="shgid" CommandArgument='<%#Eval("cdeid")%>' class="btn btn-link" Font-Bold="true" BackColor="White" ForeColor="#5ab4a1" Font-Underline="true" Style="border-radius: 100px;" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="cdoname" HeaderText="CDO"></asp:BoundField>
                    <asp:BoundField DataField="cdmname" HeaderText="CDM"></asp:BoundField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
    <br />
    <div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Data Available" EmptyDataRowStyle-CssClass="text-danger" Caption="SHGs" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped" OnPageIndexChanging="GridView1_PageIndexChanging"
                AllowPaging="true" PageSize="10" AutoGenerateColumns="true">
                <Columns>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

