﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class tasklist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select [taskid] as ID, [cdeid] as [CDE ID], name as Name, [taskname] as [Task Name], [date] as Date, [time] as Time, [branch] as [Work Details], [shglist] as [SHG List], [taskby] as [Task By] from [vikasardo].[vikasa].[tasks] where status='0' order by ID desc", gvsprovider);
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {

    }
    protected void btncancel_Click(object sender, EventArgs e)
    {

    }
    protected void Button2_ServerClick(object sender, EventArgs e)
    {

    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select [taskid] as ID, [cdeid] as [CDE ID], name as Name, [taskname] as [Task Name], [date] as Date, [time] as Time, [branch] as [Work Details], [shglist] as [SHG List], [taskby] as [Task By] from [vikasardo].[vikasa].[tasks] where status='0' order by ID desc", gvsprovider);
    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter CDE Name',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtfrom.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter From Date',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        if (txtto.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter To Date',text: '',timer: 2000,showConfirmButton: false})", true);
            return;
        }
        Glib.LoadRequest("Select [taskid] as ID, [cdeid] as [CDE ID], name as Name, [taskname] as [Task Name], [date] as Date, [time] as Time, [branch] as [Work Details], [shglist] as [SHG List], [taskby] as [Task By] from [vikasardo].[vikasa].[tasks] where status='0' and name='" + txtsrch.Text + "' and convert(datetime, date, 103) between convert(datetime, '" + txtfrom.Text + "', 103) and convert(datetime, '" + txtto.Text + "', 103) order by ID desc", gvsprovider);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select name as tmp from [vikasardo].[vikasa].[tasks] where name like '" + pre + "%' ";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
}