﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class approved_shg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg  where status='4' order by shgid desc", gvsprovider);
            Glib.LoadDDL(ddldistrict, "select distinct name from [vikasardo].[vikasa].districts", "name", "name");
            Glib.LoadDDL(ddlregion, "select distinct name from [vikasardo].[vikasa].regions", "name", "name");
            Glib.LoadDDL(ddlbranch, "select distinct name from [vikasardo].[vikasa].bankBranch", "name", "name");
            Glib.LoadDDL(ddlcde, "select * from [vikasardo].[vikasa].[cde]", "cdename", "cdeid");
        }
    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg  where status='4' order by shgid desc", gvsprovider);

    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select top 10 name as tmp from [vikasardo].[vikasa].[shg] where name like '" + pre + "%' and status='4' union Select top 10 branch as tmp from [vikasardo].[vikasa].shg where branch like '" + pre + "%' and status='4'";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            return;
        }
        else
        {
            Glib.LoadRequest("Select * from [vikasardo].[vikasa].[shg] where name like '%" + txtsrch.Text.Trim() + "%' or branch like '%" + txtsrch.Text.Trim() + "%' and status='4' order by shgid desc", gvsprovider);
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("edi"))
        {
            btnsubmit.Text = "Update";
            MultiView1.ActiveViewIndex = 0;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow rows = gvsprovider.Rows[index];
            string id = HttpUtility.HtmlDecode(rows.Cells[1].Text).ToString();
            lblid.Text = id;
            txtname.Text = HttpUtility.HtmlDecode(rows.Cells[2].Text).ToString();
            ddltype.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[3].Text).ToString();
            txtvillage.Text = HttpUtility.HtmlDecode(rows.Cells[4].Text).ToString();
            txt_taluk.Text = HttpUtility.HtmlDecode(rows.Cells[5].Text).ToString();
            ddlregion.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[6].Text).ToString();
            ddldistrict.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[7].Text).ToString();
            txtsavingaccnum.Text = HttpUtility.HtmlDecode(rows.Cells[8].Text).ToString();
            txtmeetdate.Text = HttpUtility.HtmlDecode(rows.Cells[9].Text).ToString();
            txtannualmeetdate.Text = HttpUtility.HtmlDecode(rows.Cells[10].Text).ToString();
            txtannualincome.Text = HttpUtility.HtmlDecode(rows.Cells[11].Text).ToString();
            txtmembers.Text = HttpUtility.HtmlDecode(rows.Cells[12].Text).ToString();
            txtmeetplace.Text = HttpUtility.HtmlDecode(rows.Cells[13].Text).ToString();
            txtmeetingtime.Text = HttpUtility.HtmlDecode(rows.Cells[14].Text).ToString();
            txtformdate.Text = HttpUtility.HtmlDecode(rows.Cells[15].Text).ToString();
            txtaccopendate.Text = HttpUtility.HtmlDecode(rows.Cells[16].Text).ToString();
            ddlbranch.SelectedItem.Text = HttpUtility.HtmlDecode(rows.Cells[18].Text).ToString();
            ddlcde.SelectedValue = HttpUtility.HtmlDecode(rows.Cells[19].Text).ToString();
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        Alib.idExecute("update [vikasardo].[vikasa].[shg] set name='" + txtname.Text + "', type='" + ddltype.SelectedItem.Text + "', village='" + txtvillage.Text + "', taluka='" + txt_taluk.Text + "', region='" + ddlregion.SelectedItem.Text + "', district='" + ddldistrict.SelectedItem.Text + "', savingAccount='" + txtsavingaccnum.Text + "', meetingDate='" + txtmeetdate.Text + "', annualmeetDate= '" + txtannualmeetdate.Text + "', annualIncome= '" + txtannualincome.Text + "', totalMember='" + txtmembers.Text + "', meetingPlace='" + txtmeetplace.Text + "', meetingTime='" + txtmeetingtime.Text + "', formationDate='" + txtformdate.Text + "', savingAccOpenDate='" + txtaccopendate.Text + "', cdeid='" + ddlcde.SelectedValue + "', branch='" + ddlbranch.SelectedItem.Text + "'  where shgid='" + lblid.Text + "'");
        Glib.LoadRequest("Select * from [vikasardo].[vikasa].shg  where status='4' order by shgid desc", gvsprovider);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Updated successfully..!',text: '',timer: 2000,showConfirmButton: false},function(){location.href='shg.aspx'})", true);
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
    }
}