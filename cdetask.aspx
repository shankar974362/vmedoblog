﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cdetask.aspx.cs" Inherits="tasklist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="assets/js/jquery.min.js"></script>
    <link href="assets/css/sweetalert.css" rel="stylesheet" />
    <script src="assets/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

    <link href="assets/js/jquery.timepicker.css" rel="stylesheet" />
    <script src="assets/js/jquery.timepicker.js"></script>
    <script src="assets/js/jquery.timepicker.min.js"></script>
   
    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5"
        media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
        $(function () {
            $('#<%=txtsrch.ClientID%>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "cdetask.aspx/GetInvName",
                        data: "{ 'pre':'" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (d) {
                            var data = JSON.parse(d.d);
                            console.log(data);
                            response($.map(data, function (item) {
                                return { value: item.tmp }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                }
            });
        });
    </script>
     <script type="text/javascript">
         $(function () {
             $('#bfs2').addClass('active');
             $('#<%=txtfrom.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });
            $('#<%=txtto.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });
        });
    </script>
    <style type="text/css">
        .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
    <%--<div class="row">
        <div class="col-sm-12 ">
            <p class="text-muted page-title-alt">
                <button type="button" id="Button2" style="float: right;" runat="server" class="btn btn-default waves-effect waves-light" onserverclick="Button2_ServerClick">
                    <span class="btn-label"><i class="fa fa-plus"></i>
                    </span>Create New</button>
            </p>
        </div>
    </div>--%>
    <div class="row">
        <div class="col-sm-3">
            <asp:TextBox ID="txtsrch" runat="server" CssClass="form-control" placeholder="Enter CDE Name" ></asp:TextBox>
        </div>
        <div class="col-sm-3">
            <asp:TextBox ID="txtfrom" runat="server" CssClass="form-control" placeholder="Enter From Date"></asp:TextBox>
        </div>
        <div class="col-sm-3">
            <asp:TextBox ID="txtto" runat="server" CssClass="form-control" placeholder="Enter To Date"></asp:TextBox>
        </div>
        <div class="col-sm-3">
            <asp:Button ID="btnsrch" runat="server" CssClass="btn btn-lg btn-default btn-custom" Text="Search" OnClick="btnsrch_Click" />
        </div>
    </div>
    <br />
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="row">
                <div class="col-lg-5 card-box">
                    <div class="form-group">
                        <label>Name</label>
                        <asp:TextBox ID="txtname" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Region</label>
                        <asp:DropDownList ID="ddlregion" runat="server" class="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-lg btn-default btn-custom" Text="Submit" OnClick="btnsubmit_Click" />
                        <asp:Button ID="btncancel" runat="server" CssClass="btn btn-lg btn-danger btn-custom" Text="Cancel" OnClick="btncancel_Click" />
                    </div>
                </div>
                <div class="col-lg-7">
                </div>
            </div>
            <!-- /.row -->
        </asp:View>
    </asp:MultiView>
    <div class="col-lg-12 card-box">
        <div class="table-responsive">
            <asp:GridView ID="gvsprovider" runat="server" EmptyDataText="No Data Available" EmptyDataRowStyle-CssClass="text-danger" EmptyDataRowStyle-Font-Size="Medium" CssClass="table table-bordered table-hover table-striped" OnPageIndexChanging="gvsprovider_PageIndexChanging"
                OnRowCommand="gvsprovider_RowCommand" AllowPaging="true" PageSize="10" AutoGenerateColumns="true" Caption="CDE TASKS">
                <Columns>
                    <asp:ButtonField CommandName="Del" ControlStyle-CssClass="btn btn-danger btn-custom" ButtonType="Button"
                        Text="Del" HeaderText="Del"></asp:ButtonField>
                </Columns>
                <PagerStyle CssClass="gridview"></PagerStyle>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
