﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class approved : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //    select id,referenceID,memberid,name,momdate,shgid,cdeid,amount,reason,bankbrach,split_type,shgloanamount,aadharnum,shgname,loan_enq_num,loantype,status from shgexternalloansplit where id in (SELECT max(id) from shgexternalloansplit group by  loan_enq_num) and cdeid='" + cdeid + "' and approvedstate='1' and status='2'
            Glib.LoadRequest("select id,referenceID,loan_enq_num,shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7'", gvsprovider);
        }

    }
    protected void gvsprovider_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsprovider.PageIndex = e.NewPageIndex;
        Glib.LoadRequest("select id,referenceID,loan_enq_num,shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7'", gvsprovider);


    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetInvName(string pre)
    {
        string qur = "Select name as tmp from [vikasardo].[vikasa].[shg] where name like '" + pre + "%' ";
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(Alib.conStr))
        {
            using (SqlCommand cmd = new SqlCommand(qur, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
        }
    }
    protected void gvsprovider_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void btnsrch_Click(object sender, EventArgs e)
    {
        if (txtsrch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "swal({title: 'Enter SHG Name or Branch',text: '',timer: 1000,showConfirmButton: false})", true);
            return;
        }
        else
        {
            Glib.LoadRequest("select id,referenceID,loan_enq_num,shgname,shgid,cdeid,shgloanamount,momdate,reason,bankbrach,split_type from [vikasardo].[vikasa].[shgexternalloansplit] where id in (SELECT max(id) from [vikasardo].[vikasa].shgexternalloansplit group by loan_enq_num) and status='7' and approvedstate='7' and (shgname='"+txtsrch.Text+"' or bankbrach='"+txtsrch.Text+"')", gvsprovider);
        }
    }
}